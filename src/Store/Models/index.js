import loginUser from './Login';
import userList from './UserList';
import TestModel from './Test';
import AppModel from './App';

export default {
  test: TestModel,
  login: loginUser,
  app: AppModel,
  user: userList,
};
