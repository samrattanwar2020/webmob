import {action, thunk} from 'easy-peasy';
import {
  setLoginCredentials,
  getLoginCredentials,
} from '../../Services/Keychain';
import {STATUS} from '../../Constants';
import {APP_STATE} from '../../Constants/index';
import BaseModel from './Base';
import {showErrorToast} from '../../Lib/Toast';
import {saveValue, getValue} from '../../Services/AsyncStorage';

const checkLogin = thunk(async (actions, payload, {dispatch, injections}) => {
  const {api} = injections;

  const credentials = await getLoginCredentials();
  if (credentials) {
    const data = await getValue('@userData');
    const token = JSON.parse(data)?.data?.token?.token;
    api.setAuthorizationHeader(token);
    console.log('data', data);
    actions.changeAppState(APP_STATE.PRIVATE);
    actions.mergeState(credentials);
  } else {
    actions.changeAppState(APP_STATE.PUBLIC);
  }
});

const loginUser = thunk(async (actions, payload, {dispatch}) => {
  if (!payload.username || !payload.password) {
    return;
  }
  actions.updateStatus(STATUS.FETCHING);
  let response = await setLoginCredentials(payload.username, payload.password);
  setTimeout(() => {
    actions.updateStatus(response.status ? STATUS.SUCCESS : STATUS.FAILED);
    if (!response.status) {
      console.warn(response.error);
    } else {
      actions.changeAppState(APP_STATE.PRIVATE);
    }
  }, 1000);
});

const registerUser = thunk(
  async (actions, payload, {injections, getState, getStoreState}) => {
    const {api} = injections;
    actions.mergeState({
      registerStatus: STATUS.FETCHING,
    });

    const response = await api.register(payload);
    if (response.ok) {
      await setLoginCredentials(payload.username, payload.email);
      await saveValue('@userData', JSON.stringify(response.data));
      api.setAuthorizationHeader(response.data.data.token.token);
      actions.changeAppState(APP_STATE.PRIVATE);
      actions.mergeState({
        registerStatus: STATUS.SUCCESS,
        data: response.data,
      });
    } else {
      showErrorToast(response?.data?.meta?.message);
      actions.mergeState({
        registerStatus: STATUS.FAILED,
      });
    }
  },
);

const LoginModel = {
  //include BaseModel
  ...BaseModel(),
  //include all thunks or actions defined separately
  loginUser,
  checkLogin,
  registerUser,
  appstate: APP_STATE.UNKNOWN,
  changeAppState: action((state, payload) => {
    state.appstate = payload;
  }),
  onLoginInputChange: action((state, {key, value}) => {
    state[key] = value;
  }),
};

export default LoginModel;
