import {thunk} from 'easy-peasy';
import {STATUS} from '../../Constants';
import BaseModel from './Base';
import {showErrorToast} from '../../Lib/Toast';

const getUserList = thunk(
  async (actions, payload, {injections, getState, getStoreState}) => {
    const {api} = injections;
    actions.mergeState({
      status: STATUS.FETCHING,
    });

    if (payload.page == 1) {
      actions.mergeState({
        users: [],
      });
    }

    const response = await api.getUserList(payload);
    if (response.ok) {
      const data = getState().users ? getState().users : [];
      console.log('data');
      actions.mergeState({
        status: STATUS.SUCCESS,
        lastPage: response.data.data.pagination.lastPage,
        total: response.data.data.pagination.total,
        users: [...data, ...response.data.data.users],
      });
    } else {
      showErrorToast(response?.data?.meta?.message);
      actions.mergeState({
        status: STATUS.FAILED,
      });
    }
  },
);

const LoginModel = {
  ...BaseModel(),
  getUserList,
};

export default LoginModel;
