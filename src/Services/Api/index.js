import {create} from 'apisauce';
import {BASE_URL} from '../../Config';
import apiMonitor from './Monitor';
//import setInterceptor from './Interceptor';

export const URIS = {
  REGISTER: 'users/register',
  USER_LIST: 'users/list',
};

const createApiClient = (baseURL = BASE_URL) => {
  let api = create({
    baseURL,
    headers: {
      Accept: 'application/json',
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
    },
    timeout: 15000,
  });

  api.addMonitor(apiMonitor);
  // use interceptor if using oAuth for authentication
  // setInterceptor(api);

  const setAuthorizationHeader = (access_token) =>
    api.setHeader('Authorization', 'Bearer ' + access_token);

  const register = (payload) => api.post(URIS.REGISTER, payload);
  const getUserList = (payload) => api.get(URIS.USER_LIST, payload);

  //kickoff our api functions
  return {
    // client modifiers
    setAuthorizationHeader,
    // checkAppVersion,
    register,
    getUserList,
  };
};

export default {createApiClient};
