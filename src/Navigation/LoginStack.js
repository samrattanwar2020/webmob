import React from 'react';
import Register from '../Screens/Register';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from './Routes';

const Stack = createStackNavigator();

export default (props) => {
  return (
    <Stack.Navigator
      headerMode="none"
      initialRouteName={Routes.REGISTER_SCREEN}>
      <Stack.Screen name={Routes.REGISTER_SCREEN} component={Register} />
    </Stack.Navigator>
  );
};
