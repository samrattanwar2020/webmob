/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Routes from '../Routes';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreens from '../../Screens/Home';

const Stack = createStackNavigator();

export default (props) => {
  return (
    <Stack.Navigator headerMode="screen">
      <Stack.Screen
        name={Routes.HOME_TABS}
        options={{headerShown: false}}
        component={HomeScreens}
      />
    </Stack.Navigator>
  );
};
