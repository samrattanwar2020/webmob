/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  Text,
  FlatList,
  Image,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import LoadingActionContainer from '../../Components/LoadingActionContainer';
import {Container} from '../../Components';
import {useStoreActions, useStoreState} from 'easy-peasy';
import {useState} from 'react';
import {STATUS} from '../../Constants';
import {Card} from 'react-native-paper';
import Row from '../../Components/Row';
import colors from '../../Themes/Colors';
import metrics from '../../Themes/Metrics';

const MainScreen = ({routes, navigation}) => {
  const [page, setPage] = useState(1);
  const getUserList = useStoreActions((actions) => actions.user.getUserList);
  const {status, users, lastPage, total} = useStoreState((state) => ({
    users: state.user.users,
    lastPage: state.user.lastPage,
    total: state.user.total,
    status: state.user.status,
  }));
  console.log('users', users);
  const loading = status === STATUS.FETCHING;

  useEffect(() => {
    getUserList({page});
  }, [getUserList, page]);

  const _renderItem = ({item}) => {
    return <UserItem item={item} />;
  };

  const loadMoreData = () => {
    setPage(page + 1);
  };

  return (
    <LoadingActionContainer fixed>
      <View
        style={{
          backgroundColor: colors.tiffanyBlue,
          marginTop: 10,
          height: 80,
          paddingHorizontal: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 18, color: 'white'}}>User List</Text>
      </View>
      <Container
        style={{
          padding: 10,
        }}>
        <FlatList
          data={users}
          keyExtractor={(item, i) => i + item.id + 'key'}
          contentContainerStyle={{marginTop: 10, paddingBottom: 100}}
          renderItem={_renderItem}
          onEndReached={loadMoreData}
          onEndReachedThreshold={2}
          ListFooterComponent={() => (
            <View style={{padding: 20}}>
              <TouchableOpacity activeOpacity={0.9} onPress={loadMoreData}>
                <Text>Loading...</Text>
                {loading ? (
                  <ActivityIndicator color="primary" style={{marginLeft: 8}} />
                ) : null}
              </TouchableOpacity>
            </View>
          )}
        />
      </Container>
    </LoadingActionContainer>
  );
};

const UserItem = ({item}) => {
  return (
    <Card style={{marginTop: 10, padding: 10}}>
      <Row>
        <Image
          style={{height: 50, width: 50, borderRadius: 999}}
          source={{
            uri: item.profile_pic,
          }}
        />
        <View style={{marginLeft: 10}}>
          <Text style={{fontWeight: 'bold'}}>{item.username}</Text>
          <Text>{item.email}</Text>
        </View>
      </Row>
    </Card>
  );
};

export default MainScreen;
