/* eslint-disable react-native/no-inline-styles */
import React, {useRef} from 'react';
import {Text} from 'react-native';
import {useStoreActions, useStoreState} from 'easy-peasy';
import {STATUS} from '../../Constants';
import LoadingActionContainer from '../../Components/LoadingActionContainer';
import {
  Section,
  Container,
  PasswordInputX,
  InputX,
  ButtonX,
} from '../../Components';
import Fonts from '../../Themes/Fonts';
import {useState} from 'react';
import {showErrorToast, showSuccessToast} from '../../Lib/Toast';

export default () => {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const [email, setEmail] = useState('');

  const inputUserName = useRef();
  const inputPassword = useRef();
  const inputConfirmPassword = useRef();
  const inputEmail = useRef();

  const {status} = useStoreState((state) => ({
    status: state.login.status,
  }));

  const registerUser = useStoreActions((actions) => actions.login.registerUser);
  const register = () => {
    if (!username) {
      showErrorToast('Please enter username');
    } else if (Helper.isEmailValid(email)) {
      showErrorToast('Please enter valid email address');
    } else if (!password) {
      showErrorToast('Please enter password');
    } else if (password !== confirmPassword) {
      showErrorToast('Password and confirm password must be same');
    } else {
      registerUser({username, email, password});
    }
  };

  const loading = status === STATUS.FETCHING;

  return (
    <Container>
      <LoadingActionContainer>
        <Section>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 30,
              marginTop: 60,
              fontFamily: Fonts.type.italic,
              marginBottom: 20,
            }}>
            {'webmob'}
          </Text>
        </Section>
        <Section>
          <Text
            style={{
              fontSize: 20,
              textAlign: 'center',
              fontFamily: Fonts.type.italic,
            }}>
            Provide any details to continue
          </Text>
        </Section>
        <Section>
          <InputX
            label="USER NAME"
            mode="outlined"
            ref={inputUserName}
            style={{backgroundColor: '#fafafa', marginTop: 10}}
            autoCapitalize="none"
            returnKeyType={'next'}
            onSubmitEditing={() => inputEmail.current.focus()}
            onChangeText={(text) => setUserName(text.trim())}
            value={username}
          />
          <InputX
            label="EMAIL ID"
            mode="outlined"
            ref={inputEmail}
            style={{backgroundColor: '#fafafa', marginTop: 10}}
            autoCapitalize="none"
            returnKeyType={'next'}
            onChangeText={setEmail}
            onSubmitEditing={() => inputPassword.current.focus()}
            value={email}
          />
          <PasswordInputX
            ref={inputPassword}
            value={password}
            mode="outlined"
            style={{backgroundColor: '#fafafa', marginTop: 10}}
            label="PASSWORD"
            returnKeyType={'next'}
            onSubmitEditing={() => inputConfirmPassword.current.focus()}
            onChangeText={setPassword}
          />
          <PasswordInputX
            ref={inputConfirmPassword}
            value={confirmPassword}
            mode="outlined"
            style={{backgroundColor: '#fafafa', marginTop: 10}}
            label="CONFIRM PASSWORD"
            returnKeyType={'done'}
            onChangeText={setConfirmPassword}
          />
        </Section>
        <Section>
          <ButtonX
            loading={loading}
            dark={true}
            onPress={register}
            label="Register"
          />
        </Section>
      </LoadingActionContainer>
    </Container>
  );
};

export class Helper {
  static isEmailValid(email) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return reg.test(email) == 0;
  }
}
